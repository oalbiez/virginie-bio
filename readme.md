Salutation, toi qui me lis. Je vais te raconter ma renaissance. L'expérience qui m'a reconnecté à mon corps et mes émotions malgré la maladie et mes opérations. Mais il faut avant tout que tu sache qui je suis.


Je m'appelle Virginie Albiez, née Guitton. Je suis née à Nevers dans la Nièvre, le 3 juin 1985. J'ai vécu jusqu'à mes 25 ans dans divers villes ou villages de la Nièvre. Enfance heureuse à la campagne avec mon père et ma mère. A mes 8 ans cependant, ma mère quitte mon père pour aller vivre avec une femme et là l'enfer de mon adolescence commença.